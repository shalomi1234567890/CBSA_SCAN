#ifndef HEADER_FILE
#define HEADER_FILE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <Windows.h>

#define SIZE_A 20
#define SIZE_B 50
#define LINE_MAX_BUFFER_SIZE 255
#define PATH_VAR_F_PRI 2
char debugMsg[1024] = ""; // buffer for debug message


int runExternalCommand(char* cmd, char lines[][LINE_MAX_BUFFER_SIZE]) {
    FILE* fp;
    char path[LINE_MAX_BUFFER_SIZE];

    /* Open the command for reading. */
    fp = _popen(cmd, "r");
    if (fp == NULL) {
        return -1;
    }

    int cnt = 0;
    while (fgets(path, sizeof(path), fp) != NULL) {
        strcpy(lines[cnt++], path);
    }
    _pclose(fp);
    return cnt;
}


void debugStr(char* varName, char* var) {
    snprintf(debugMsg, sizeof(debugMsg), "%s%s=%s\n", debugMsg,
        varName, var);
}

void debugInt(char* varName, int   var) {
    snprintf(debugMsg, sizeof(debugMsg), "%s%s=%d\n", debugMsg,
        varName, var);
}




void clean2dAraay(char arrayChar[][SIZE_B])
{
    int k, j = 0;

    for (k = 0; k < SIZE_A; ++k) {
        for (j = 0; j < SIZE_B; ++j) {
            arrayChar[k][j] = 0;
        }
    }
}

void cleanAraay(char arrayChar[])
{
    int k = 0;
    for (k = 0; k < SIZE_A; ++k) {
        arrayChar[k] = 0;
    }
}
void copyArray(char arrayChar[][SIZE_B], char nameArray[])
{
    int k = 0;
    for (k = 1; k < SIZE_A; ++k) {
        if (arrayChar[k][0] != '\0')
        {
            strcat(nameArray, arrayChar[k]);
        }
    }
};
int charcount(FILE* const fin, char arrayChar[][SIZE_B])
{
    int c, count;
    int row = 0;
    int col = 0;
    count = 0;
    for (;; )
    {
        c = fgetc(fin);
        //if (c >= ) continue;
        if (c == EOF || c == '\n')
            break;
        if (c == '\t')
        {
            if (row < SIZE_A)
            {
                arrayChar[row][col] = '\0';
                ++row;
                col = 0;
            }
            continue;
        }

        if (c != '\0' && c > '\0')
        {
            if (row < SIZE_A && col < SIZE_B)
            {
                arrayChar[row][col] = c;
                ++col;
                ++count;
            }
        }

    }

    return count;
};
#endif
